import axios from 'axios';

const axiosInstance = axios.create({
    baseURL: 'https://react-my-burger-1571d.firebaseio.com/'
});

export default axiosInstance;