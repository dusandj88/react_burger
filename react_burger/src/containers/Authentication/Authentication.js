import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import {updateObject} from '../../shared/utility';
import Input from '../../components/UI/Input/Input';
import Button from '../../components/UI/Button/Button';
import Spinner from '../../components/UI/Spinner/Spinner';
import classes from './Authentication.css';
import * as actions from '../../store/actions/index';


class Authentication extends Component {
    state = {
        controls: {
            email: {
                elementType: 'input',
                elementConfiguration: {
                    type: 'email',
                    placeholder: 'Email Address'
                },
                value: '',
                validation: {
                    required: true,
                    isEmail: true
                },
                valid: false,
                touched: false
            },
            password: {
                elementType: 'input',
                elementConfiguration: {
                    type: 'password',
                    placeholder: 'Password'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 6
                },
                valid: false,
                touched: false
            }
        },
        isSingup: true
    }

    componentDidMount() {
        if (!this.props.buildingBurger && this.props.authenticationRedirectPath !== '/') {
            this.props.onSetAuthenticationRedirectPath();
        }
    }

    checkValidity(value, rules) {
        let isValid = true;
        if (!rules) {
            return true;
        }

        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if (rules.minLength) {
            isValid = value.length >= rules.minLength && isValid
        }

        if (rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid
        }

        if (rules.isEmail) {
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            isValid = pattern.test(value) && isValid
        }

        if (rules.isNumeric) {
            const pattern = /^\d+$/;
            isValid = pattern.test(value) && isValid
        }

        return isValid;
    }

    inputChangedHandler = (event, controlName) => {
        const updatedControls = updateObject(this.state.controls, {
            [controlName]: updateObject(this.state.controls[controlName], {
                value: event.target.value,
                valid: this.checkValidity(event.target.value, this.state.controls[controlName].validation),
                touched: true
            })
        });
        this.setState({ controls: updatedControls });
    }

    submitHandler = (event) => {
        event.preventDefault();
        this.props.onAuthentication(this.state.controls.email.value, this.state.controls.password.value, this.state.isSingup);
    }

    switchAuthenticationModeHandler = () => {
        this.setState(previousState => {
            return { isSingup: !previousState.isSingup }
        });
    }

    render() {
        const formElements = [];
        for (let key in this.state.controls) {
            formElements.push({
                id: key,
                configuration: this.state.controls[key]
            });
        }

        let form = formElements.map(
            formElement => (
                <Input
                    key={formElement.id}
                    elementType={formElement.configuration.elementType}
                    elementConfiguration={formElement.configuration.elementConfiguration}
                    value={formElement.configuration.value}
                    invalid={formElement.configuration.invalid}
                    shouldValidate={formElement.configuration.shouldValidate}
                    touched={formElement.configuration.touched}
                    changed={(event) => this.inputChangedHandler(event, formElement.id)} />
            )
        );

        if (this.props.loading) {
            form = <Spinner />;
        }

        let errorMessage = null;

        if (this.props.error) {
            errorMessage = (
                <p>{this.props.error.message}</p>
            );
        }

        let authenticationRedirect = null; 

        if (this.props.isAuthenticated) {
            authenticationRedirect = <Redirect to={this.props.authenticationRedirectPath }/>;
        }

        return (
            <div className={classes.Authentication}>
                {authenticationRedirect}
                {errorMessage}
                <form onSubmit={this.submitHandler}>
                    {form}
                    <Button buttonType="Success">Submit</Button>
                </form>
                <Button buttonType="Danger" clicked={this.switchAuthenticationModeHandler}>SWITCH TO {this.state.isSingup ? "SINGIN" : "SINGUP"}</Button>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        loading: state.authentication.loading,
        error: state.authentication.error,
        isAuthenticated: state.authentication.token !== null,
        buildingBurger: state.burgerBuilder.building,
        authenticationRedirectPath: state.authentication.authenticationRedirectPath
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onAuthentication: (email, password, isSingup) => dispatch(actions.authentication(email, password, isSingup)),
        onSetAuthenticationRedirectPath: () => dispatch(actions.setAuthenticationRedirectPath('/'))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Authentication);