import * as actionTypes from './actionTypes';
import axiosInstance from 'axios';

export const authenticationStart = () => {
    return {
        type: actionTypes.AUTHENTICATION_START
    };
};

export const authenticationSuccess = (token, userId) => {

    return {
        type: actionTypes.AUTHENTICATION_SUCCESS,
        idToken: token,
        userId: userId
    };
};

export const authenticationFailed = (error) => {
    return {
        type: actionTypes.AUTHENTICATION_FAILED,
        error: error
    };
};

export const logout = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('expirationDate');
    localStorage.removeItem('localId');
    return {
        type: actionTypes.AUTHENTICATION_LOGOUT
    };
};

export const checkAuthenticationTimeout = (expirationTime) => {
    return dispatch => {
        setTimeout(() => { 
            dispatch(logout());
        }, expirationTime * 1000);
    }
}

export const authentication = (email, password, isSignup) => {
    return dispatch => {
        dispatch(authenticationStart());
        const authenticationData = {
            email: email,
            password: password,
            returnSecureToken: true
        };

        let url = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=AIzaSyCYBJoEvV-8QZxJ9zglN6xn1eyZf-uktzU';

        if (!isSignup) {
            url = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyCYBJoEvV-8QZxJ9zglN6xn1eyZf-uktzU';
        }

        axiosInstance.post(url, authenticationData)
            .then(response => {
                console.log(response);
                localStorage.setItem('token', response.data.idToken);
                const expirationDate = new Date(new Date().getTime() + response.data.expiresIn * 1000);
                localStorage.setItem('expirationDate', expirationDate);
                localStorage.setItem('userId', response.data.localId);
                dispatch(authenticationSuccess(response.data.idToken, response.data.localId));
                dispatch(checkAuthenticationTimeout(response.data.expiresIn));
            })
            .catch(error => {
                dispatch(authenticationFailed(error.response.data.error));
            });
    };
}; 

export const setAuthenticationRedirectPath = (path) => {
    return {
        type : actionTypes.SET_AUTHENTICATION_REDIRECT_PATH,
        path: path
    };
};

export const authenticationCheckState = () => {
    return dispatch => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(logout());
        } else {
            const expirationDate = new Date(localStorage.getItem('expirationDate'));
            if (expirationDate <= new Date()) {
                dispatch(logout());
            } else {
                const userId = localStorage.getItem('localId');
                dispatch(authenticationSuccess(token, userId));
                dispatch(checkAuthenticationTimeout((expirationDate.getTime() - new Date().getTime()) / 1000));
            }
        }
    };
}