import reducer from './authentication';
import * as actionTypes from '../actions/actionTypes';

describe('authentication reducer', () => {
    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual({
            token: null,
            userId: null,
            error: null,
            loading: false,
            authenticationRedirectPath: '/'
        });
    })

    it('should store token upon login', () => {
        expect(reducer({
            token: null,
            userId: null,
            error: null,
            loading: false,
            authenticationRedirectPath: '/'
        }, {
            type: actionTypes.AUTHENTICATION_SUCCESS,
                token: 'some-token',
                userId: 'some-user-id'
            })).toEqual({
                token: 'some-token',
                userId: 'some-user-id',
                error: null,
                loading: false,
                authenticationRedirectPath: '/'
            });
    });
})