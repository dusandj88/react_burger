import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';
import asynchronousComponent from './hoc/AsynchronousComponent/AsynchronousComponent';
import Layout from './hoc/Layout/Layout'
import BurgerBuilder from './containers/BurgerBuilder/BurgerBuilder';
import Logout from './containers/Authentication/Logout/Logout';
import * as actions from './store/actions/index';

const asynchronousCheckout = asynchronousComponent(() => {
  return import('./containers/Checkout/Checkout');
});

const asynchronousOrders = asynchronousComponent(() => {
  return import('./containers/Orders/Orders');
});

const asynchronousAuthentication = asynchronousComponent(() => {
  return import('./containers/Authentication/Authentication');
});

class App extends Component {
  componentDidMount() {
    this.props.onTryAutoSignup();
  }

  render() {
    let routes = (
      <Switch>
        <Route path="/authentication" component={asynchronousAuthentication} />
        <Route path="/" exact component={BurgerBuilder} />
        <Redirect to="/" />
      </Switch>
    );

    if (this.props.isAuthenticated) {
      routes = (
        <Switch>
          <Route path="/orders" component={asynchronousOrders} />
          <Route path="/checkout" component={asynchronousCheckout} />
          <Route path="/authentication" component={asynchronousAuthentication} />
          <Route path="/logout" component={Logout} />
          <Route path="/" exact component={BurgerBuilder} />
          <Redirect to="/" />
        </Switch>
      );
    }

    return (
      <div>
        <Layout>
          {routes}
        </Layout>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.authentication.token !== null
  };
};

export const mapDispatchToProps = dispatch => {
  return {
    onTryAutoSignup: () => dispatch(actions.authenticationCheckState())
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
